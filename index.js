var express    = require('express');        // call express
var app        = express();                 // define our app using express
var bodyParser = require('body-parser');
const { Router, application } = require('express');
var beautify = require("json-beautify");
const fs = require('fs');
// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use('/', (req, res, next) => {
let obj = {};
obj.ipAddr =  req.headers['x-forwarded-for'] || req.socket.remoteAddress 
obj.url = req.originalUrl;
obj.method = req.method;
obj.headers = req.headers
obj.baseUrl = req.baseUrl
obj.body = req.body;
obj.ip = req.ip;
req.subdomains = req.subdomains;

const str = beautify(obj,null,2,100)
fs.writeFileSync(`./reqs/`+ new Date().getTime() + ".json", str);
res.json(obj);
}) 
const PORT =  process.env.PORT || 3000 
app.listen(PORT, ()=> {

})


